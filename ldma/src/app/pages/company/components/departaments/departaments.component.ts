import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Departament } from '../../../../models/departament';
import { DepartamentService } from '../../../../services/departament.service';

import { SaveDepartament } from './modals/saveDepartament/saveDepartament.component';
import { DeleteDepartament } from './modals/deleteDepartament/deleteDepartament.component';

@Component({
  selector: 'departaments',
  templateUrl: './departaments.html',
  styleUrls: ['./departaments.scss']
})
export class Departaments {

  private data: [Departament]; 
  private filterQuery = "";
  private rowsOnPage = 10;

  constructor(private departamentService: DepartamentService,
              private modalService: NgbModal) { }
  
  ngOnInit(){  
    this.loadList();
  }
  
  loadList(){
    this.data = null;
    this.departamentService.list().subscribe(list => {
      if(list){
        this.data = list;
      }
    });
  }

  onCreate() {
    const activeModal = this.modalService.open(SaveDepartament, { size: 'lg' });
    activeModal.result.then( _ => {
      this.loadList();
    });
    activeModal.componentInstance.departament = new Departament();
    activeModal.componentInstance.modalHeader = 'New departament';
  }

  onEdit(row) {
    const activeModal = this.modalService.open(SaveDepartament, { size: 'lg' });
    activeModal.result.then( _ => {
      this.loadList();
    });
    activeModal.componentInstance.departament = row;
    activeModal.componentInstance.modalHeader = 'Edit departament';
  }

  onDelete(row){
    const activeModal = this.modalService.open(DeleteDepartament, { size: 'sm' });
    activeModal.result.then( _ => {
      this.loadList();
    });
    activeModal.componentInstance.departament = row;
    activeModal.componentInstance.modalHeader = 'Delete departament';
  }

}
