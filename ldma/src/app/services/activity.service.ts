import { Inject, Injectable } from "@angular/core";
import { Http, RequestOptions, Response, URLSearchParams, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


import { Activity } from "../models/activity";
import { environment } from '../../environments/environment';
import { CommonService } from "../services/common.service";

const _backendUri = environment.backendUri;

@Injectable()
export class ActivityService {

	private _headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    private _options = new RequestOptions({ headers: this._headers });

    constructor(private _http: Http,
                private _commonService: CommonService) {}
    
    list(limit: number): Observable<[Activity]> {
        var url = `${_backendUri}/activity?company_id=${this._commonService.company_id}`;
        url += `&limit=${limit}`;

        var date = new Date();
        var dateString = date.getFullYear() + ("0" + (date.getMonth() + 1)).slice(-2) + ("0" + date.getDate()).slice(-2) + ("0" + date.getHours() + 1 ).slice(-2) + ("0" + date.getMinutes()).slice(-2) + ("0" + date.getSeconds()).slice(-2);

        url += `&init_date=${dateString}`;

        return this._http
                    .get(url)
                    .map((data: Response): any => {
                        return this._commonService.extractList(data, new Activity())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }

    get(id: string): Observable<Activity> {
        return this._http
                    .get(`${_backendUri}/activity/${id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Activity())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    
   
    save(activity: Activity): Observable<Activity> {
        return this._http
                    .post(`${_backendUri}/activity`, activity, this._options)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Activity())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }     
    
    delete(id: string): Observable<Activity> {
        return this._http
                    .delete(`${_backendUri}/activity/${id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Activity())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }   

}