import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';

import { Departament } from '../../../../../../models/Departament';
import { DepartamentService } from '../../../../../../services/departament.service';

@Component({
  selector: 'deleteDepartament',
  templateUrl: './deleteDepartament.html'
})
export class DeleteDepartament {

    private modalHeader: string;

    private departament: Departament;
    private errorMessage: string;

    constructor(private activeModal: NgbActiveModal,
                private departamentService: DepartamentService) { }

    delete() {
        this.departamentService.delete(this.departament.id)
            .subscribe(
                departament => {
                    this.activeModal.close();
                },
                err => {
                    console.log(err);
                    this.errorMessage = err;
                }
            )
    }

    cancel() {        
        this.activeModal.close();
    }

}