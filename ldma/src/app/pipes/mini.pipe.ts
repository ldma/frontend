import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'mini', pure: true})
export class MiniPipe implements PipeTransform {

  constructor() {}

  transform(value: string, num: number): string {
      return value.substring(0,num) + "...";
  }

}