import { NgModule }      from '@angular/core';
import { CommonModule, DatePipe }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { routing } from './company.routing';
import { ApplicationPipes } from '../../app.pipes.module';
import { DataTableModule } from "angular2-datatable";

import { Company } from './company.component';
import { Departaments } from './components/departaments/departaments.component';
import { Employees } from './components/employees/employees.component';

import { EmployeeService } from '../../services/employee.service';
import { DepartamentService } from '../../services/departament.service';

import { DatumPipe } from '../../pipes/datum.pipe';
import { SafeHtml } from '../../pipes/safehtml.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
    ApplicationPipes,
    DataTableModule
  ],
  declarations: [
    Company,
    Departaments,
    Employees
  ],
  providers: [
    DatePipe,
    DatumPipe,
    SafeHtml,
    EmployeeService,
    DepartamentService
  ]
})
export class CompanyModule {}
