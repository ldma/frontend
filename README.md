# LDMA Frontend

Front For the Final Project of MOBILE STARTUP ENGINEERING BOOTCAMP IV

Devoloped with Angular 2 and Typescript

![](http://www.masterangular.com/images/angular2typescript.png)

## Test
cd /ldma
Run `npm start`

## Build
Run `npm run build` to build the project.

## S3 hosting
After build upload all files of the folder **dist** to the S3 bucket: **com.ldma.frontend**

## S3 Image Hosting
All profile images are hosted in a S3 Bucket (com.ldma.profile)

## Public url
[https://s3.eu-west-2.amazonaws.com/com.ldma.frontend/index.html][10]

## TODO List

- Providers web
- Add employees to departaments
- Creating Notifications (Push&Mailing)

## TEAM

- [Eric Risco][1]
- [Begoña Hormaechea][2]
- [Alberto Galera][3]
- [Paco Cardenal][4]
- [Eugenio Barquín][5]

[1]: https://github.com/eriscoand
[2]: https://github.com/begohorma
[3]: https://github.com/albertgs
[4]: https://github.com/pacocardenal
[5]: https://github.com/ebarquin

[10]:	https://s3.eu-west-2.amazonaws.com/com.ldma.frontend/index.html
