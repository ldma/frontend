import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule }  from '@angular/common';

import { DataFilterPipe } from './pipes/datafilter.pipe';
import { DatumPipe } from './pipes/datum.pipe';
import { MiniPipe } from './pipes/mini.pipe';
import { SafeHtml } from './pipes/safehtml.pipe';
import { S3ImageBucket } from './pipes/s3imagebucket.pipe';
import { EmployeesInvited } from './pages/company/components/employees/components/employees-invited/employees-invited.component';



@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        MiniPipe,
        DatumPipe,
        DataFilterPipe,
        SafeHtml,
        S3ImageBucket,
        EmployeesInvited
    ],
    exports: [
        MiniPipe,
        DatumPipe,
        DataFilterPipe,
        SafeHtml,
        S3ImageBucket,
        EmployeesInvited
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ApplicationPipes {}