import { Routes, RouterModule }  from '@angular/router';

import { SingleActivity } from './activity.component';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: SingleActivity,
    children: []
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
