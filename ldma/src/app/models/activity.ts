import { jsonable } from './interface/jsonable';
import { Employee } from './employee';
import { CommonService } from '../services/common.service';

var commonService = new CommonService();

export class Activity implements jsonable<Activity> {

    public id: string;
    public company_id: string;
    public owner_id: string;
    public owner: Employee;
    public invited: Array<Employee>;
    public isPrivate: boolean;
    public name: string;
    public description: string;
    public init_date: string;
    public end_date: string;
    public latitude: number;
    public longitude: number;

    fromJson(json: any): Activity {
        var activity = new Activity();
        activity.id = json.id;
        activity.company_id = json.company_id || commonService.company_id;
        activity.owner_id = json.owner_id || commonService.owner_id;
        activity.owner = json.owner != null ? new Employee().fromJson(json.owner) : null;
        activity.invited = json.invited != null ? new Employee().fromJsonToList(json.invited) : new Array<Employee>();
        activity.isPrivate = json.isPrivate;
        activity.name = json.name;
        activity.description = json.description;
        activity.init_date = json.init_date;
        activity.end_date = json.end_date;
        activity.latitude = json.latitude || commonService.initial_latitude;
        activity.longitude = json.longitude || commonService.initial_longitude;
        return activity;
    }

    fromJsonToList(json: any[]): Activity[] {
        return json.reduce((activities: Activity[], activity: Activity) => {
            activities.push(this.fromJson(activity));
            return activities;
        }, []);
    }


}