import { jsonable } from './interface/jsonable';

export class Employee implements jsonable<Employee> {

    public id: string;
    public email: string;
    public avatar_url: string;
    public company_id: string;
    public name: string;
    public surnames: string;
    public birthday_date: string;
    
    fromJson(json: any): Employee {
        var employee = new Employee();
        employee.id = json.id;
        employee.email = json.email;
        employee.avatar_url = json.avatar_url || "ldma-keepcoding.png";
        employee.company_id = json.company_id || "1";
        employee.name = json.name;
        employee.surnames = json.surnames;
        employee.birthday_date = json.birthday_date;
        return employee;
    }

    fromJsonToList(json: any[]): Employee[] {
        return json.reduce((employees: Employee[], employee: Employee) => {
            employees.push(this.fromJson(employee));
            return employees;
        }, []);
    }

}