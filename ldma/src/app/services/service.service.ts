import { Inject, Injectable } from "@angular/core";
import { Http, RequestOptions, Response, URLSearchParams, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BaThemePreloader} from '../theme/services';

import { Service } from "../models/service";
import { CommonService } from "../services/common.service";
import { environment } from '../../environments/environment';

const _backendUri = environment.backendUri;

@Injectable()
export class ServiceService {

	private _headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    private _options = new RequestOptions({ headers: this._headers });

    constructor(private _http: Http,
                private commonService: CommonService) {}
    
                list(): Observable<[Service]> {
                    return this._http
                                .get(`${_backendUri}/service`, this._options)
                                .map((data: Response): any => {
                                    return this.commonService.extractList(data, new Service())
                                })
                                .catch(this.commonService.handleErrorObservable);
                }
            

    get(id: number): Observable<Service> {
        return this._http
                    .get(`${_backendUri}/service/?id=${id}`)
                    .map((data: Response): any => {
                        this.commonService.extractData(data, new Service())
                    })
                    .catch(this.commonService.handleErrorObservable);
    }    
    // TODO: Arreglarlo
   
    save(service: Service): Observable<Service> {
        return this._http
                    .post(`${_backendUri}/service`, service, this._options)
                    .map((data: Response): any => {
                        this.commonService.extractData(data, new Service())
                    })
                    .catch(this.commonService.handleErrorObservable)
    }    

}