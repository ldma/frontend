import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../../app.translation.module';
import { NgaModule } from '../../../theme/nga.module';
import { Daterangepicker } from 'ng2-daterangepicker';

import { SaveActivity } from './saveActivity.component';

import { ActivityService } from '../../../services/activity.service';
import { DatumPipe } from '../../../pipes/datum.pipe';
import { ApplicationPipes } from '../../../app.pipes.module';

import { AgmCoreModule } from '@agm/core';
import { SelectModule } from 'ng2-select';
import { routing } from './saveActivity.routing';

@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAcHLWW2GLIiNYvbU68Jh37UqefjhCXdpU'
    }),
    SelectModule,
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
    Daterangepicker,
    ApplicationPipes
  ],
  declarations: [
    SaveActivity
  ],
  providers: [
    ActivityService,
    DatePipe,
    DatumPipe
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class SaveActivityModule {}
