import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'register',
  templateUrl: './register.html',
  styleUrls: ['./register.scss']
})
export class Register implements OnInit {

    // submitted: boolean = false;

  confirmUser = false;
  createdUser = false;
  didFail = false;
  @ViewChild('usrForm') form: NgForm;

  constructor(private authService: AuthService) {}

  ngOnInit() {
      this.didFail = false;
      this.authService.authDidFail.subscribe(
          (didFail: boolean) => this.didFail = didFail
      );
  }

  onSubmit() {
      this.didFail = false;
      // const userName = this.form.value.username;
      const email = this.form.value.email;
      const password = this.form.value.password;
      // this.authService.signUp(userName, email, password);
      this.authService.signUp(email, email, password, 'admin');
      this.createdUser = true;


  }
  
  onDoConfirm() {
    this.confirmUser = true;
  }

  onConfirm( formValue: {usrName: string, validationCode: string }) {
    this.authService.confirmUser(formValue.usrName, formValue.validationCode);
  }

}
