import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';

import { Departament } from '../../../../../../models/Departament';
import { DepartamentService } from '../../../../../../services/departament.service';

@Component({
  selector: 'saveDepartament',
  templateUrl: './saveDepartament.html'
})
export class SaveDepartament {

    private modalHeader: string;

    private departament: Departament;
    private errorMessage: string;

    constructor(private activeModal: NgbActiveModal,
                private departamentService: DepartamentService) { }

    saveChanges() {
        this.departamentService.save(new Departament().fromJson(this.departament))
            .subscribe(
                departament => {
                    this.activeModal.close();
                },
                err => {
                    console.log(err);
                    this.errorMessage = err;
                }
            )
    }

    closeModal() {
        this.activeModal.close();
    }

}