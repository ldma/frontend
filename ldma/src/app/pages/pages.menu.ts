export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'activities',
        data: {
          menu: {
            title: 'Activities',
            icon: 'ion-android-calendar',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'company',
        data: {
          menu: {
            title: 'Company',
            icon: 'ion-briefcase',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      }
    ]
  }
];
