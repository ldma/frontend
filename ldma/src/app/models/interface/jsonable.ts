export interface jsonable<T> {
    fromJson(json: any): T;
    fromJsonToList(json: any[]): T[];
}