import { Pipe, PipeTransform } from '@angular/core';

import { environment } from '../../environments/environment';

@Pipe({name: 's3ImageBucket', pure: true})
export class S3ImageBucket implements PipeTransform {

  constructor() {}

  transform(value: string): string {
      return environment.s3Url + value;
  }

}