import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {UsersService} from '../../services/users.service';
import {CognitoUser} from 'amazon-cognito-identity-js';

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss'],
})
export class Login implements OnInit {

  isAuthenticated = false;
  didFail = false;


  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;

  constructor(fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private usersService: UsersService) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  ngOnInit() {
    this.authService.authDidFail.subscribe(
          (didFail: boolean) => this.didFail = didFail
      );

    this.authService.authStatusChanged.subscribe(
        (authenticated) => {
          this.isAuthenticated = authenticated;
          if (authenticated) {

              const cUser: CognitoUser = this.authService.getAuthenticatedUser();
              this.usersService.initUser(cUser); // create user in user table if necessary

              // TODO: identificar tipo de usuario para determinar la página a cargar
              // ahora navegar al dashboard
              this.router.navigate(['/pages/dashboard']); // ¿con /?
          }else {
          }
        },
    );
    // comprobar si el usuario ya está autenticado
    this.authService.initAuth();
  }

  public onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
        this.authService.signIn(this.email.value, this.password.value);
    }
  }

  onLogout() {
    this.authService.logout();
  }
}
