import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DatePipe } from '@angular/common'

import { Activity } from '../../../../models/Activity';
import { ActivityService } from '../../../../services/activity.service';

import { DatumPipe } from '../../../../pipes/datum.pipe';

@Component({
  selector: 'timeline',
  templateUrl: './timeline.html',
  styleUrls: ['./timeline.scss']
})
export class Timeline {

  private activityList: [Activity];

  constructor(private activityService: ActivityService,
              private datumPipe: DatumPipe) { }

  ngOnInit() {
    this.activityService.list(4)
      .subscribe(
        activityList => {
          this.activityList = activityList;
        }
      ),
        err => {
          console.log(err);
        }
  }

}
