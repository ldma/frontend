import { Routes, RouterModule } from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from '../services/auth-guard.service';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path : '', 
    redirectTo: 'login', 
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module#LoginModule',
  },
  {
    path: 'register',
    loadChildren: 'app/pages/register/register.module#RegisterModule',
  },
    {
        path: 'forgotPassword',
        loadChildren: 'app/pages/forgot/forgotPassword.module#ForgotPasswordModule',
    },
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'dashboard', canActivate: [AuthGuard], loadChildren: './dashboard/dashboard.module#DashboardModule' },

      { path: 'activities', canActivate: [AuthGuard], loadChildren: './activities/activities.module#ActivitiesModule' },
      { path: 'activity/:id', canActivate: [AuthGuard], loadChildren: './activities/activity/activity.module#ActivityModule' },
      { path: 'saveActivity/:id', canActivate: [AuthGuard], loadChildren: './activities/saveActivity/saveActivity.module#SaveActivityModule' },

      { path: 'company', canActivate: [AuthGuard], loadChildren: './company/company.module#CompanyModule' }
    ],
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
