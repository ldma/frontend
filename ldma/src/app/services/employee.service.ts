import { Inject, Injectable, Output } from "@angular/core";
import { Http, RequestOptions, Response, URLSearchParams, Headers, RequestMethod} from "@angular/http";

import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Employee } from "../models/employee";
import { CommonService } from "../services/common.service";
import { environment } from '../../environments/environment';

const _backendUri = environment.backendUri;

@Injectable()
export class EmployeeService {

    constructor(private _http: Http,
                private _commonService: CommonService) { }
    
    list(): Observable<[Employee]> {
        return this._http
                    .get(`${_backendUri}/employee?company_id=${this._commonService.company_id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractList(data, new Employee())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }

    get(id: string): Observable<Employee> {
        return this._http
                    .get(`${_backendUri}/employee/${id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Employee())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    
    
    save(employee: Employee): Observable<Employee> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http
                    .post(`${_backendUri}/employee`, employee, options)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Employee());
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    
    
    delete(id: string): Observable<Employee> {
        return this._http
                    .delete(`${_backendUri}/employee/${id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Employee())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    

}