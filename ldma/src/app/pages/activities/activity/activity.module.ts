import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule, DatePipe }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../../app.translation.module';
import { NgaModule } from '../../../theme/nga.module';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DatumPipe } from '../../../pipes/datum.pipe';
import { ApplicationPipes } from '../../../app.pipes.module';

import { SingleActivity } from './activity.component';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ActivityService } from '../../../services/activity.service';

import { AgmCoreModule } from '@agm/core';

import { routing } from './activity.routing';

@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAcHLWW2GLIiNYvbU68Jh37UqefjhCXdpU'
    }),
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
    ApplicationPipes
  ],
  declarations: [
    SingleActivity
  ],
  providers: [
    ActivityService,
    DatePipe,
    DatumPipe
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class ActivityModule {}
