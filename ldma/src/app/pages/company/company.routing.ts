import { Routes, RouterModule }  from '@angular/router';

import { Company } from './company.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
  {
    path: '',
    component: Company,
    children: [ ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
