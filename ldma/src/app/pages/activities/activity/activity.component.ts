import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivityService } from '../../../services/activity.service';
import { Router, ActivatedRoute } from '@angular/router';

import { Activity } from '../../../models/activity';

@Component({
  selector: 'activity',
  styleUrls: ['./activity.scss'],
  templateUrl: './activity.html',
})
export class SingleActivity {

    private sub: any;
    private id: string;
    
    private activity: Activity = new Activity();
    private markers = [];
    public selectedItems:Array<any>;
  
    constructor(
        private activityService: ActivityService,
        private route: ActivatedRoute,
        private router: Router) { }  

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = params['id']; 
            this.activityService.get(this.id).subscribe(activity => {      
                this.activity = activity;

                this.markers = [];
                this.markers.push({
                    latitude: this.activity.latitude,
                    longitude: this.activity.longitude
                });

                this.selectedItems = [];
                this.activity.invited.forEach(employee => {
                this.selectedItems.push({
                    id: employee.id,
                    text: employee.name + " " + employee.surnames
                })
                });
            },
            err => {
                this.router.navigateByUrl('/pages/dashboard');
            });
        });
    }

}
