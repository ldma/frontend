import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Activity } from '../../models/activity';
import { ActivityService } from '../../services/activity.service';

import { DatumPipe } from '../../pipes/datum.pipe';

import { DeleteActivity } from './modals/deleteActivity/deleteActivity.component';

@Component({
  selector: 'activities',
  styleUrls: ['./activities.scss'],
  templateUrl: './activities.html',
})
export class Activities {

  private data: [Activity];
  private filterQuery = "";
  private rowsOnPage = 10;
  
  constructor(private activityService: ActivityService,
              private datumPipe: DatumPipe,
              private modalService: NgbModal) { }   

    ngOnInit(){  
      this.loadList();
    }
    
    loadList(){
      this.data = null;
      this.activityService.list(100).subscribe(list => {
        if(list){
          this.data = list;
        }
      });
    }

    onDelete(row){
      const activeModal = this.modalService.open(DeleteActivity, { size: 'sm' });
      activeModal.result.then( _ => {
        this.loadList();
      });
      activeModal.componentInstance.activity = row;
      activeModal.componentInstance.modalHeader = 'Delete activity';
    }

    changeDatum(date: string){
      return this.datumPipe.transform(date);
    }
 
}
