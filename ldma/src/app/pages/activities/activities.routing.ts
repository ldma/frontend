import { Routes, RouterModule }  from '@angular/router';

import { Activities } from './activities.component';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from '../../services/auth-guard.service';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: Activities,
    children: []
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
