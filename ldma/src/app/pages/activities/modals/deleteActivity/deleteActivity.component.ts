import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';

import { Activity } from '../../../../models/Activity';
import { ActivityService } from '../../../../services/activity.service';

@Component({
  selector: 'deleteActivity',
  templateUrl: './deleteActivity.html'
})
export class DeleteActivity {

    private modalHeader: string;

    private activity: Activity;
    private errorMessage: string;

    constructor(private activeModal: NgbActiveModal,
                private activityService: ActivityService) { }

    delete() {
        this.activityService.delete(this.activity.id)
            .subscribe(
                activity => {
                    this.activeModal.close();
                },
                err => {
                    console.log(err);
                    this.errorMessage = err;
                }
            )
    }

    cancel() {        
        this.activeModal.close();
    }

}