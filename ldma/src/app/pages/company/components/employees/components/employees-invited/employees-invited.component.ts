import { Component, Input } from '@angular/core';

import { Employee } from '../../../../../../models/employee';

@Component({
 selector: 'employees-invited',
 templateUrl: './employees-invited.html',
 styleUrls: ['./employees-invited.scss']
})
export class EmployeesInvited {
  private _employees: [Employee];
 
  @Input()
  set employees(employees: [Employee]) {
    this._employees = employees;
  }

  get employees(){
    return this._employees;
  }
 
}