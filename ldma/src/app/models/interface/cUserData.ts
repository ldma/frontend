export interface cUserData {
    userName: string;
    email: string;
    password: string;
    profile: string;
}