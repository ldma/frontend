import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs/Rx';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';

import { Employee } from '../../../../../../models/Employee';
import { EmployeeService } from '../../../../../../services/employee.service';
import { CommonService } from '../../../../../../services/common.service';

import { EmailValidator } from '../../../../../../theme/validators';

import { environment } from '../../../../../../../environments/environment';

require('aws-sdk/dist/aws-sdk');

@Component({
  selector: 'saveEmployee',
  templateUrl: './saveEmployee.html'
})
export class SaveEmployee {

    private form:FormGroup;

    private modalHeader: string;

    private employee: Employee;
    private errorMessage: string;
    
    private email:AbstractControl;

    private image: string;
    private file: any;

    constructor(private activeModal: NgbActiveModal,
                private employeeService: EmployeeService,
                private commonService: CommonService,
                private fb: FormBuilder) { 
        this.form = fb.group({
            'email': ['', Validators.compose([Validators.required, EmailValidator.validate])]
        });
        this.email = this.form.controls['email'];
    }

    ngOnInit(){  
        this.image = environment.s3Url + this.employee.avatar_url;
    }

    fileEvent(fileInput: any){

        let fileList: FileList = fileInput.target.files;
        if(fileList.length > 0) {
            let file: File = fileList[0];
            var img: any = document.querySelector("#preview img");
            img.file = file;
            this.file = fileInput.target.files[0] || null;

            var reader = new FileReader();
            reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
            reader.readAsDataURL(file);
        }
    }

    saveChanges() {
        if (this.form.valid) {

            this.employeeService.save(new Employee().fromJson(this.employee))
                .subscribe( employee => {
                    if(this.file == null){
                        this.activeModal.close();
                    }else{
                        this.commonService.uploadCoverS3(employee.id, this.file).then( fileName => {
                            employee.avatar_url = fileName;
                            this.employeeService.save(employee).subscribe( _ =>{
                                this.activeModal.close();
                            }, err => { this.errorMessage = err });
                        }).catch( err => {
                            console.log(err);
                            this.errorMessage = err;
                        });
                    }
                },
                err => {
                    console.log(err);
                    this.errorMessage = err;
                }
            )
        }else{
            this.errorMessage = "Email not valid...";
        }
    }

    closeModal() {
        this.activeModal.close();
    }

}