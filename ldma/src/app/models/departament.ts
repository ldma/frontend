import { jsonable } from './interface/jsonable';

export class Departament implements jsonable<Departament> {

    public id: string;
    public name: string;
    public description: string;
    public company_id: string;
        
    fromJson(json: any): Departament {
        var departament = new Departament();
        departament.id = json.id;
        departament.company_id = "1";
        departament.name = json.name;
        departament.description = json.description;
        return departament;
    }

    fromJsonToList(json: any[]): Departament[] {
        return json.reduce((departaments: Departament[], departament: Departament) => {
            departaments.push(this.fromJson(departament));
            return departaments;
        }, []);
    }

}