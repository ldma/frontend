import { NgModule }      from '@angular/core';
import { CommonModule, DatePipe }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { routing } from './dashboard.routing';
import { ApplicationPipes } from '../../app.pipes.module';

import { Dashboard } from './dashboard.component';
import { Timeline } from './components/timeline/timeline.component';
import { ActivityService } from '../../services/activity.service';

import { DatumPipe } from '../../pipes/datum.pipe';
import { MiniPipe } from '../../pipes/mini.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
    ApplicationPipes
  ],
  declarations: [
    Dashboard,
    Timeline
  ],
  providers: [
    DatePipe,
    DatumPipe,
    MiniPipe,
    ActivityService
  ]
})
export class DashboardModule {}
