import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DatePipe } from '@angular/common'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Employee } from '../../../../models/employee';
import { EmployeeService } from '../../../../services/employee.service';

import { SaveEmployee } from './modals/saveEmployee/saveEmployee.component';
import { DeleteEmployee } from './modals/deleteEmployee/deleteEmployee.component';

@Component({
  selector: 'employees',
  templateUrl: './employees.html',
  styleUrls: ['./employees.scss']
})
export class Employees {
  
  private data: [Employee];
  private filterQuery = "";
  private rowsOnPage = 10;

  constructor(private employeeService: EmployeeService,
              private modalService: NgbModal) { }
  
  ngOnInit(){  
    this.loadList(); 
  }
  
  loadList(){
    this.data = null;
    this.employeeService.list().subscribe(list => {
      if(list){
        this.data = list;
      }
    });
  }
  
  onCreate() {
    const activeModal = this.modalService.open(SaveEmployee, { size: 'lg' });
    activeModal.result.then( _ => {
      this.loadList();
    });
    activeModal.componentInstance.employee = new Employee().fromJson(new Employee());
    activeModal.componentInstance.modalHeader = 'New employee';
  }

  onEdit(row) {
    const activeModal = this.modalService.open(SaveEmployee, { size: 'lg' });
    activeModal.result.then( _ => {
      this.loadList();
    });
    activeModal.componentInstance.employee = row;
    activeModal.componentInstance.modalHeader = 'Edit employee';
  }

  onDelete(row){
    const activeModal = this.modalService.open(DeleteEmployee, { size: 'sm' });
    activeModal.result.then( _ => {
      this.loadList();
    });
    activeModal.componentInstance.employee = row;
    activeModal.componentInstance.modalHeader = 'Delete employee';
  }

}
