import { NgModule, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
/*
 * Platform and Environment providers/directives/pipes
 */
import { routing } from './app.routing';

// App is our top level component
import { App } from './app.component';
import { AppState, InternalStateType } from './app.service';
import { GlobalState } from './global.state';
import { NgaModule } from './theme/nga.module';
import { PagesModule } from './pages/pages.module';

import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { CommonService } from './services/common.service';
import { UsersService } from './services/users.service';
import { DepartamentService } from './services/departament.service';
import { ActivityService } from './services/activity.service';
import { EmployeeService } from './services/employee.service';

import { SaveDepartament } from './pages/company/components/departaments/modals/saveDepartament/saveDepartament.component';
import { DeleteDepartament } from './pages/company/components/departaments/modals/deleteDepartament/deleteDepartament.component';

import { SaveEmployee } from './pages/company/components/employees/modals/saveEmployee/saveEmployee.component';
import { DeleteEmployee } from './pages/company/components/employees/modals/deleteEmployee/deleteEmployee.component';

import { ApplicationPipes } from './app.pipes.module';

import { DeleteActivity } from './pages/activities/modals/deleteActivity/deleteActivity.component';

// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState,
  CommonService,
  DepartamentService,
  ActivityService,
  EmployeeService,
  AuthService,
  AuthGuard,
  UsersService
];

export type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void,
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [App],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [
    App,
    SaveDepartament,
    DeleteDepartament,
    DeleteActivity,
    SaveEmployee,
    DeleteEmployee
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    HttpModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule.forRoot(),
    NgbModule.forRoot(),
    PagesModule,
    ApplicationPipes,
    routing
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    APP_PROVIDERS,
  ],
  entryComponents: [
    SaveDepartament,
    DeleteDepartament,
    DeleteActivity,
    SaveEmployee,
    DeleteEmployee
  ],
})

export class AppModule {

  constructor(public appState: AppState) {
  }
}
