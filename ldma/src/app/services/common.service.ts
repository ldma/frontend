import { Inject, Injectable } from "@angular/core";
import { Http, RequestOptions, Response, URLSearchParams, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { jsonable } from '../models/interface/jsonable';

import { environment } from '../../environments/environment';

@Injectable()
export class CommonService {

    public company_id = "1";
    public initial_latitude: number = 42.5055122;
    public initial_longitude: number = 1.5179623;
    public owner_id = "536f79b4-4e2f-49a5-adf4-3473e578aa92";

    handleErrorObservable (error: Response | any) {
        let errorMessage = error.json && error.json() != null && error.json().errorMessage != null ? error.json().errorMessage : 'Server error...';
        return Observable.throw(errorMessage);
    }

    extractData<T extends jsonable<T>>(res: Response, obj: T) {
        let body = res.json();
        return obj.fromJson(body) || {};
    }

    extractList<T extends jsonable<T>>(res: Response, obj: T) {
        let body = res.json();
        body = body.Items != null ? body.Items : body;
        return obj.fromJsonToList(body) || [];
    }

    uploadCoverS3(id, file): Promise<string> {  
        return new Promise((resolve, reject) => {
            var AWSService = window.AWS;
    
            AWSService.config.accessKeyId = environment.s3AccessKeyId;
            AWSService.config.secretAccessKey = environment.s3SecretAccessKey;
            AWSService.config.region = environment.s3Region;
    
            var bucket = new AWSService.S3({params: {Bucket: environment.s3Bucket}});
    
            var params = {Key: id + "." + file.name.split(".")[1], Body: file};
    
            bucket.upload(params, function (err, data) {
                if(err) reject(err)
                resolve(data.Key);
            });  
        });

    }

}
