import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
    AuthenticationDetails, CognitoUser, CognitoUserAttribute, CognitoUserPool,
    CognitoUserSession,
} from 'amazon-cognito-identity-js';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../models/user';
import { environment } from '../../environments/environment';
import {cUserData} from '../models/interface/cUserData';

const POOL_DATA = {
    UserPoolId: environment.userPoolId,
    ClientId: environment.clientId,
};

const userPool = new CognitoUserPool(POOL_DATA);

@Injectable()
export class AuthService {
    authStatusChanged = new Subject<boolean>();
    authDidFail = new BehaviorSubject<boolean>(false);

    registeredUser: CognitoUser;

    constructor(private router: Router) {}

    signUp(username:string, email: string, password: string, profile: string): void {

        const user: cUserData = {
            userName: username,
            email: email,
            password: password,
            profile: profile
        };

        const attrList: CognitoUserAttribute[] = [];
        const emailAtrribute = {
            Name: 'email',
            Value: user.email,
        };

        const profileAtrribute = {
            Name: 'profile',
            Value: user.profile,
        };
        attrList.push(new CognitoUserAttribute(emailAtrribute));
        attrList.push(new CognitoUserAttribute(profileAtrribute));

        userPool.signUp(user.email, user.password, attrList, null, (err, result) => {
            if (err) {
                this.authDidFail.next(true);
                return;
            }
           // alert ( ' usuario registrado. Continuar con la verificación');
            this.authDidFail.next(false);
            this.registeredUser = result.user;
        });
        return;
    }

    confirmUser(username: string, code: string) {
        const userData = {
            Username: username,
            Pool: userPool,
        };

        const cognitoUser = new CognitoUser( userData );
        cognitoUser.confirmRegistration( code, true, (err, result) => {
          if (err) {
              this.authDidFail.next(true);
              return;
          }
          this.authDidFail.next(false);


          this.router.navigate(['/']); // Ir a la pagina principal -> login
        });

    }

    signIn(username: string, password: string): void {

        const authData = {
            Username: username,
            Password: password,
        };

        const authDetails = new AuthenticationDetails( authData);

        const userData = {
            Username: username,
            Pool: userPool,
        };
        const cognitoUser = new CognitoUser(userData);
        const that = this;
        cognitoUser.authenticateUser(authDetails, {
            onSuccess(result: CognitoUserSession) {
                that.authStatusChanged.next(true);
                that.authDidFail.next(false);
            },
            onFailure(err) {
                console.log(err);
                alert(' ERROR!: '+ err);
                that.authDidFail.next(true);
            },
        });
        return;
    }

    getAuthenticatedUser() {
        return userPool.getCurrentUser();
    }

    logout() {
        this.getAuthenticatedUser().signOut();
        this.authStatusChanged.next(false);
    }

    forgotPassword(username: string) {
        console.log('en forgotPassword');

        const userData = {
            Username: username,
            Pool: userPool,
        };

        const cognitoUser = new CognitoUser( userData );
        const that = this;
        cognitoUser.forgotPassword({
            onSuccess() {
                that.authDidFail.next(false);
            },
            onFailure(err) {
                console.log(err);
                that.authDidFail.next(true);
            },
            inputVerificationCode() {
                that.authDidFail.next(false);
            }
        });
        return;
    }

    confirmNewPassword(username: string, password: string, verificationCode: string) {
        console.log('en confirmNewPassword');

        const userData = {
            Username: username,
            Pool: userPool,
        };

        const cognitoUser = new CognitoUser( userData );
        const that = this;
        cognitoUser.confirmPassword(verificationCode, password, {
            onSuccess() {
                alert("Password succesfully changed!");
                that.authDidFail.next(false);
                that.router.navigate(['/']); // Ir a la página de login
            },
            onFailure(err) {
                console.log(err);
                that.authDidFail.next(true);
            }
        });
    }

    isAuthenticated(): Observable<boolean> {
        const user = this.getAuthenticatedUser();
        const obs = Observable.create((observer) => {
            if (!user) {
                observer.next(false);
            }else {
                user.getSession((err, session) => {
                    if (err) {
                        observer.next(false);
                    }else {
                        if (session.isValid()) {
                            observer.next(true);
                        }else {
                            observer.next(false);
                        }
                    }
                });
            }
            observer.complete();
        });
        return obs;
    }

    initAuth() {
        this.isAuthenticated().subscribe(
            (auth) => this.authStatusChanged.next(auth)
        );
    }
}