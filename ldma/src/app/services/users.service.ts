
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { CognitoUser } from 'amazon-cognito-identity-js';

import 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {

    constructor(private http: Http) {}

  //  private url = 'https://ozk6fvd056.execute-api.eu-west-2.amazonaws.com/dev/apiv1/user';

    initUser(cUser: CognitoUser) {

        // obtner la sesion del usuario
        // si session ok obtener los atributos del usuario : email, profile
        // comprobar que no hay una registro con el id/email? del usuario
        // aún no sé que id le va a poner cognito y que atributo es
        // si no existe el registro (body null) hacer post para crear el usuario
        // después redirigir a dashboard (o página correspondiente)

        cUser.getSession((err, session) => {
            if (err) {
                console.log(err);
            }
            cUser.getUserAttributes(( err, result) => {
                if (err) {
                    console.log(err);
                    return;
                }else {

                    const userData = {
                        email: result [3].getValue(),
                        profile: result[2].getValue(),
                    };


                    const token =  session.getIdToken().getJwtToken();
                    
                    const queryParam = '?email='+ userData.email;

                    this.http.get(environment.backendUri+'/user' + queryParam)
                        .subscribe(
                            (response: Response) => {
                                const data = response.json();
                                
                                if (data[0] === undefined) {
                                    this.http.post(environment.backendUri + '/user', JSON.stringify(userData), {
                                        headers: new Headers({
                                            'Authorization': session.getIdToken().getJwtToken(),
                                            'Content-Type': 'application/json',
                                        }),
                                    })
                                        .subscribe(
                                            (result) => {
                                            },
                                            (error) => {
                                                console.log(error);
                                            },
                                        );
                                }
                                else{
                                }
                            },
                            (error) => {
                                console.log(error);
                            },
                        );
                }
            });
        });

    }


}