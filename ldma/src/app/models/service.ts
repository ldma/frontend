import { jsonable } from './interface/jsonable';

export class Service implements jsonable<Service> {

    public id: string;
    public name: string;
    public description: string;
    public provider_id: string;
    public cover_url: string;
    public thumb_url: string;
    public gps_lat: string;
    public gps_long: string;


    fromJson(json: any): Service {
        var service = new Service();
        service.id = json.id;
        service.name = json.name;
        service.description = json.description;
        service.provider_id = json.provider_id;
        service.cover_url = json.cover_url;
        service.thumb_url = json.thumb_url;
        service.gps_lat = json.gps_lat;
        service.gps_long = json.gps_long;
        return service;
    }

    fromJsonToList(json: any[]): Service[] {
        return json.reduce((services: Service[], service: Service) => {
            services.push(this.fromJson(service));
            return services;
        }, []);
    }


}