import { NgModule } from '@angular/core';
import { ForgotPassword } from './forgotPassword.component';
import { routing } from './forgotPassword.routing';
import { NgaModule } from '../../theme/nga.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
        AppTranslationModule,
        ReactiveFormsModule,
        FormsModule,
        NgaModule,
        routing,
    ],
    declarations: [
        ForgotPassword,
    ],
})
export class ForgotPasswordModule {}