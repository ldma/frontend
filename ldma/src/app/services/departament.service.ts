import { Inject, Injectable, Output } from "@angular/core";
import { Http, RequestOptions, Response, URLSearchParams, Headers, RequestMethod} from "@angular/http";

import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Departament } from "../models/departament";
import { CommonService } from "../services/common.service";
import { environment } from '../../environments/environment';

const _backendUri = environment.backendUri;

@Injectable()
export class DepartamentService {

    constructor(private _http: Http,
                private _commonService: CommonService) { }
    
    list(): Observable<[Departament]> {
        return this._http
                    .get(`${_backendUri}/departament?company_id=1`)  //TODO: put company_id in a session var
                    .map((data: Response): any => {
                        return this._commonService.extractList(data, new Departament())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }

    get(id: number): Observable<Departament> {
        return this._http
                    .get(`${_backendUri}/departament/${id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Departament())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    
    
    save(departament: Departament): Observable<Departament> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http
                    .post(`${_backendUri}/departament`, departament, options)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Departament())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    
    
    delete(id: string): Observable<Departament> {
        return this._http
                    .delete(`${_backendUri}/departament/${id}`)
                    .map((data: Response): any => {
                        return this._commonService.extractData(data, new Departament())
                    })
                    .catch(error => {
                        return this._commonService.handleErrorObservable(error);
                    });
    }    

}