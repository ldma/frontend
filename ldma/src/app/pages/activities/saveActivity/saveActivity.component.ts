import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DatePipe } from "@angular/common";
import { Router, ActivatedRoute } from '@angular/router';
import { DaterangePickerComponent } from 'ng2-daterangepicker';

import { Activity } from '../../../models/Activity';
import { Employee } from '../../../models/Employee';

import { ActivityService } from '../../../services/activity.service';
import { EmployeeService } from '../../../services/employee.service';

import { CommonService } from '../../../services/common.service';
import { DatumPipe } from '../../../pipes/datum.pipe';

@Component({
  selector: 'save-activity',
  templateUrl: './saveActivity.html',
  styleUrls: ['./saveActivity.scss']
})
export class SaveActivity {

  @ViewChild(DaterangePickerComponent)
  private picker: DaterangePickerComponent;

  private activity: Activity = new Activity();
  private errorMessage: String;

  private sub: any;
  private id: string;

  private latitude: number;
  private longitude: number;
  private markers = [];

  public items:Array<any>;
  public selectedItems:Array<any>;

  constructor(
    private activityService: ActivityService,
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router,
    private datumPipe: DatumPipe,
    private elementRef: ElementRef,
    private commonService: CommonService) {

      this.latitude = Number(this.commonService.initial_latitude);
      this.longitude = Number(this.commonService.initial_longitude);
      this.markers = [];
      this.markers.push({
        latitude: this.latitude,
        longitude: this.longitude
      });

    }  
    
  ngOnInit() {

    this.employeeService.list().subscribe(employees => {
      this.items = [];
      employees.forEach(employee => {
        this.items.push({
          id: employee.id,
          text: employee.name + " " + employee.surnames
        })
      });
    });

    this.sub = this.route.params.subscribe(params => {
        this.id = params['id']; 
        if(this.id == "0"){
          this.activity = new Activity().fromJson(new Activity()); //🚬 🚬
        }else{
          this.activityService.get(this.id).subscribe(activity => {   
              this.activity = activity;

              this.latitude = this.activity.latitude;
              this.longitude = this.activity.longitude;

              this.markers = [];
              this.markers.push({
                latitude: this.activity.latitude,
                longitude: this.activity.longitude
              });

              this.selectedItems = [];
              this.activity.invited.forEach(employee => {
                this.selectedItems.push({
                  id: employee.id,
                  text: employee.name + " " + employee.surnames
                })
              });

              this.activity.init_date = this.datumPipe.getFormatDaterange(activity.init_date);
              this.activity.end_date = this.datumPipe.getFormatDaterange(activity.end_date);

              this.picker.datePicker.setStartDate(activity.init_date);
              this.picker.datePicker.setEndDate(activity.end_date);
          },
          err => {
              this.router.navigateByUrl('/pages/dashboard');
          });
        }
    });
  }

  changeMarker($event){

    this.markers = [];
    this.markers.push({
      latitude: $event.coords.lat,
      longitude: $event.coords.lng
    });

    this.activity.latitude = $event.coords.lat;
    this.activity.longitude = $event.coords.lng;
  }

  selectedDate(value: any, datepicker?: any) {
      var datePipe = new DatePipe("en");
      this.activity.init_date = datePipe.transform(value.start);
      this.activity.end_date = datePipe.transform(value.end);
  }
 
  save() {
    this.activityService.save(new Activity().fromJson(this.activity))
      .subscribe( 
        activity => {
          this.router.navigateByUrl('/pages/activities');
      },
        err => {
          console.log(err);
          this.errorMessage = err;
        });
  }

  cancel(){
    this.router.navigateByUrl('/pages/activities');
  }

  selected(value:any):void {
    let employee = new Employee();
    employee.id = value.id;
    this.activity.invited == undefined ? [] : this.activity.invited;
    this.employeeService.get(employee.id).subscribe(employee_db => {
      this.activity.invited.push(employee_db);
      this.activity.invited = this.activity.invited.slice();
    })
  }

  removed(value:any):void {
    let employee = new Employee();
    employee.id = value.id;
    this.activity.invited == undefined ? [] : this.activity.invited;
    for(var i = 0; i<this.activity.invited.length; i++){
      if(employee.id == this.activity.invited[i].id){
        this.activity.invited.splice(i,1);
      }
    }  
  }


}
