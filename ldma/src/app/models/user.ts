import { jsonable } from './interface/jsonable';


export class User implements jsonable<User> {

    public id: string;
    public email: string;
    public isAdmin: boolean;
    public isProvider: boolean;
    public isDeleted: boolean;

    fromJson(json: any): User {
        const user = new User();
        user.id = json.id;
        user.email = json.email;
        user.isAdmin = json.isAdmin === 'true' ? true : false;
        user.isProvider = json.isProvider === 'true' ? true : false;
        user.isDeleted = json.isDeleted === 'true' ? true : false;
        return user;
    }

    fromJsonToList(json: any []): User[] {
        return json.reduce((users: User[], user: any) => {
            users.push(this.fromJson(user));
        }, []);
    }
}

