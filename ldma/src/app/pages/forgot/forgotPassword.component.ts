import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {runInThisContext} from 'vm';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'forgotPassword',
    templateUrl: './forgotPassword.html',
    styleUrls: ['./forgotPassword.scss'],
})

export class ForgotPassword  implements  OnInit {

    confirmPassword = false;
    didFail = false;
    @ViewChild('fgForm') form: NgForm;
    email;

    constructor(private authService: AuthService) {}

ngOnInit() {
    this.didFail = false;
    console.log('forgot password 1');
    this.authService.authDidFail.subscribe(
        (didFail: boolean) => this.didFail = didFail
    );
}

onSubmit() {
    this.didFail = false;
    console.log('onSubmit fgForm');
    this.email = this.form.value.email;
    this.authService.forgotPassword(this.email);
    this.confirmPassword = true;
}

onConfirm(formValue: {newPassword: string, validationCode: string}){
    console.log(' confirmar password. password:'+ formValue.newPassword + 'code ' + formValue.validationCode);
    this.authService.confirmNewPassword(this.email,formValue.newPassword,formValue.validationCode);
}
}