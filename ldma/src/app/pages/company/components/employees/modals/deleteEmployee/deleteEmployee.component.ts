import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';

import { Employee } from '../../../../../../models/Employee';
import { EmployeeService } from '../../../../../../services/employee.service';

@Component({
  selector: 'deleteEmployee',
  templateUrl: './deleteEmployee.html'
})
export class DeleteEmployee {

    private modalHeader: string;

    private employee: Employee;
    private errorMessage: string;

    constructor(private activeModal: NgbActiveModal,
                private employeeService: EmployeeService) { }

    delete() {
        this.employeeService.delete(this.employee.id)
            .subscribe(
                employee => {
                    this.activeModal.close();
                },
                err => {
                    console.log(err);
                    this.errorMessage = err;
                }
            )
    }

    cancel() {        
        this.activeModal.close();
    }

}