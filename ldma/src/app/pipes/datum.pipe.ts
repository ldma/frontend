import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe }  from '@angular/common';

@Pipe({name: 'datum', pure: true})
export class DatumPipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {}

  transform(value: string): string {
    if(value == null) return "";
    let date: Date = new Date(this.getCorrectFormat(value));
    if(value != "Invalid Date"){
        return this.datePipe.transform(date.toString(), 'medium')
    }else{
        return "";
    }
  }

  getCorrectFormat(value: string): string {
    return value.substr(0,4) + "/" + 
        value.substr(4,2) + "/" +
        value.substr(6,2) + " " +
        value.substr(8,2) + ":" +
        value.substr(10,2) + ":" +
        value.substr(12,2);
  }

  getFormatDaterange(value: string): string {
    return value.substr(4,2) + "/" +
            value.substr(6,2) + "/" +
            value.substr(0,4) + " " +
            value.substr(8,2) + ":" +
            value.substr(10,2) + ":" +
            value.substr(12,2);
  }


}