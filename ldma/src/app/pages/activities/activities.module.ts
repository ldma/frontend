import { NgModule }      from '@angular/core';
import { CommonModule, DatePipe }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { Daterangepicker } from 'ng2-daterangepicker';
import { ApplicationPipes } from '../../app.pipes.module';

import { Activities } from './activities.component';
import { DataTableModule } from "angular2-datatable";
import { ActivityService } from '../../services/activity.service';

import { routing } from './activities.routing';

import { DatumPipe } from '../../pipes/datum.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
    Daterangepicker,
    DataTableModule,
    ApplicationPipes
  ],
  declarations: [
    Activities
  ],
  providers: [
    ActivityService,
    DatePipe,
    DatumPipe
  ]
})
export class ActivitiesModule {}
